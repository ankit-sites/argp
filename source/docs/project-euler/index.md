---
title: Project Euler
subtitle: Project Euler Problems
---

[Argp](/index.html) / [Docs](/docs/index.html) / Project Euler

[![GitHub last commit](https://img.shields.io/github/last-commit/ankitrgadiya/project-euler.svg)](https://github.com/ankitrgadiya/project-euler/commits)
[![GitHub language count](https://img.shields.io/github/languages/count/ankitrgadiya/project-euler.svg)](https://github.com/ankitrgadiya/project-euler)
[![GitHub stars](https://img.shields.io/github/stars/ankitrgadiya/project-euler.svg)](https://github.com/ankitrgadiya/project-euler/stargazers)
[![GitHub](https://img.shields.io/github/license/ankitrgadiya/project-euler.svg)](https://github.com/ankitrgadiya/project-euler/blob/master/LICENSE)

## About Project Euler

[Project Euler](https://projecteuler.net/) is a project by volunteers which has
a large archive of mathematical problems ranging from easy to hard level.
Programmers are to write code in any programming language to solve the problem
and find the correct solution. Every problem has a thread associated with it
where users can post their code and check code of fellow users.

## Problems

The following list contains problems that I’ve solved with the link to the
explanation. If you’ve not tried it on your own, I would recommend giving it a
try by yourself first before looking at the solution.

* &#x2714; [Problem 1](/docs/project-euler/prob1.html)
* &#x2714; [Problem 2](/docs/project-euler/prob2.html)

## Links

* [Source Code](https://github.com/ankitrgadiya/project-euler/)

## License

[BSD
3-Clause](https://github.com/ankitrgadiya/project-euler/blob/master/LICENSE)
License.
