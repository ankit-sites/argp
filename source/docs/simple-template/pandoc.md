---
title: "Simple Template &mdash; Pandoc"
subtitle: "Pandoc Simple Template"
---

[Argp](/index.html) / [Docs](/docs/index.html) / [Simple
Template](/docs/simple-template/index.html) / Pandoc


[![GitHub tag](https://img.shields.io/github/tag-date/simple-template/pandoc.svg)](https://github.com/simple-template/pandoc)
[![Travis (.org)](https://img.shields.io/travis/simple-template/pandoc.svg)](https://travis-ci.org/simple-template/pandoc)
[![GitHub issues](https://img.shields.io/github/issues/simple-template/pandoc.svg)](https://github.com/simple-template/pandoc/issues)
[![GitHub stars](https://img.shields.io/github/stars/simple-template/pandoc.svg)](https://github.com/simple-template/pandoc/stargazers)
[![GitHub last commit](https://img.shields.io/github/last-commit/simple-template/pandoc.svg)](https://github.com/simple-template/pandoc/commits/master)
[![GitHub license](https://img.shields.io/github/license/simple-template/pandoc.svg)](https://github.com/simple-template/pandoc/blob/master/LICENSE)

Demo: <https://st.argp.in/pandoc/>

## About

Pandoc is the universal document converter. This project uses Pandoc to
generate static web pages from markdown files using Make. The `source`
directory contains markdown source files. Make is configured to check for the
folders and create them under `html` directory and generate *HTML* files from
the source using Pandoc using templates in template directory.

## Usage

The main part is `Makefile` and `template` directory. Easy way to get started
is to clone the [repository](https://github.com/simple-template/pandoc/) and
edit the files as desired. After placing the source files in `source`
directory, run `make` command to generate *HTML* files.

```bash
$ make
```

You can open the generated files directly in your web browers. Alternatively,
you can installed Caddy or any other web server and point it to `html`
directory. Caddy server is recommended because it ships as a single binary
file which can be easily installed. Usage is also very simple with minimal
configuration. To run Caddy and serve the files run the following command.

```bash
$ make server
```

## Sitemap and Tree

By default Make will generate `sitemap.txt` containing sitemap of all pages in
`html` directory. To configure the *baseurl* for sitemap, change the `BASEURL`
variable at the top of `Makefile`. To change the filename you can edit the
`SITEMAP` variable under *Files* section. If you don't want it to generate
sitemap automatically, then remove `$(SITEMAP)` from default target's
prerequisite list.

Make uses `tree` command to generate `sitemap.html` containing list of all
*HTML* files in form of a tree. By default, `navigation.html` contains a link
to this file. If you don't want it to generate this file, then remove `$(TREE)`
from default target's prerequisite list and remove the `<a>` tag from
`navigation.html`.

## Static Assets

Make looks for files in `static` directory and copies the contents into `html`
folder. This folder can be used to put files like `robots.txt`, *CSS*,
*Javascript*, images or basically any static content. Note that even if a
single file is changed, Make will copy all of the contents to `html` file.

## Navigation

For navigation, it template uses `template/navigation.html` file. The contents
of the file are directly pasted in `header` tag above `h1` tag. To change the
links you can directly edit the file. After editing the file if you will run
`make`, it will automatically update all of the *HTML* files.

## CI/CD

If you want to use CI/CD services to generate the static site automatically,
you can use the [Pandoc docker](/docs/docker/pandoc.html) image I maintain.  It
is configured to work well with this project.

```yaml
# Sample Gitlab CI
image: ankitrgadiya/pandoc

pages:
  script:
  - make all
  - mv html public
  artifacts:
    paths:
    - public
  only:
  - master
```
```yaml
# Sample Travis CI
sudo: required

services:
  - docker

before_install:
- docker pull ankitrgadiya/pandoc

script:
- |
  docker run \
  -v $TRAVIS_BUILD_DIR:/dir \
  -it ankitrgadiya/pandoc \
  /bin/sh -c "cd dir && make"
deploy:
    - provider: pages
      skip-cleanup: true
      github-token: $GITHUB_TOKEN
      local-dir: html
      on:
        branch: master
```

## Links

* [Caddy](https://caddyserver.com/)
* [Make](https://www.gnu.org/software/make/)
* [Markdown](https://en.wikipedia.org/wiki/Markdown)
* [Pandoc](https://pandoc.org/)
* [Source](https://github.com/simple-template/pandoc)

## License

[BSD 3-Clause](https://github.com/simple-template/pandoc/blob/master/LICENSE)
License.
