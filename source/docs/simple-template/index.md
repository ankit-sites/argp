---
title: "Simple Template"
subtitle: "Simple yet fully functional templates for static site generators."
---

[Argp](/index.html) / [Docs](/docs/index.html) / Simple Template

Simple template project aims at making fully functional templates for commonly
used static site generators. The design lay the focus on content while keeping
the aesthetics to an absolute minimum. It does not use bulky *Javascript* and
*CSS* frameworks to make the site look better and responsive. Instead, it uses
the fact that the barebone *HTML* itself is responsive. Therefore, the
templates do not use *Javascript* at all and very minimal *CSS*. All these
combined results in very fast websites. The websites work fine in both command
line and graphical web browsers on any device which supports internet. Simple
Template is a proud of [Viewable With Any
Browser](https://www.anybrowser.org/campaign/) campaign.

The Simple template project is inspired by [Eric S.
Raymond](http://catb.org/~esr/)’s [HTML
Hell](http://catb.org/esr/html-hell.html) article and [txti](http://txti.es/)
by [Barry T. Smith](https://twitter.com/thebarrytone).

* [Jekyll](/docs/simple-template/jekyll.html)
* [Pandoc](/docs/simple-template/pandoc.html)

## Links

* [Github:simple-template](https://github.com/simple-template/)
