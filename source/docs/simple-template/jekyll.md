---
title: "Simple Template &mdash; Jekyll"
subtitle: "Jekyll simple template"
---

[Argp](/index.html) / [Docs](/docs/index.html) / [Simple
Template](/docs/simple-template/index.html) / Jekyll

[![Gem](https://img.shields.io/gem/v/jekyll-simple-template.svg)](https://rubygems.org/gems/jekyll-simple-template)
[![Gem](https://img.shields.io/gem/dt/jekyll-simple-template.svg)](https://rubygems.org/gems/jekyll-simple-template)
[![Travis (.org)](https://img.shields.io/travis/simple-template/jekyll.svg)](https://travis-ci.org/simple-template/jekyll)
[![GitHub issues](https://img.shields.io/github/issues/simple-template/jekyll.svg)](https://github.com/simple-template/jekyll/issues)
[![GitHub stars](https://img.shields.io/github/stars/simple-template/jekyll.svg)](https://github.com/simple-template/jekyll/stargazers)
[![GitHub last commit](https://img.shields.io/github/last-commit/simple-template/pandoc.svg)](https://github.com/simple-template/pandoc/commits/master)
[![GitHub license](https://img.shields.io/github/license/simple-template/jekyll.svg)](https://github.com/simple-template/jekyll/blob/master/LICENSE)


Demo:  <https://st.argp.in/jekyll/>

## Usage

If you are using Jekyll with [Github Pages](https://pages.github.com/), you can
simply add the following line in your site's `_config.yml`.

```yaml
remote_theme: simple-template/jekyll
```

The template is also available as Ruby
[Gem](https://rubygems.org/gems/jekyll-simple-template/). To add the Gem to
your jekyll site do the following:

* Add the Gem in the `Gemfile`.
  ```ruby
  source "https://rubygems.org"
  "jekyll-simple-template"
  ```
* Add the Theme in `_config.yml`.
  ```yaml
  theme: jekyll-simple-template
  ```

Alternatively, you can fork the
[repository](https://github.com/simple-template/jekyll) and use it as your
website.

## Plugins

Instead of inventing the wheel, the template uses the following popular plugins
for Jekyll, to add features to the template:

* [jekyll-feed](https://github.com/jekyll/jekyll-feed): Generates the Atom (RSS
  like) Feed for Blog Posts
* [jekyll-seo-tag](https://github.com/jekyll/jekyll-seo-tag): Adds necesarry
  SEO information in the head tag
* [jekyll-sitemap](https://github.com/jekyll/jekyll-sitemap): Generates
  sitemap.org compliant sitemap for the website

**Note**: For Github Pages users, all the plugins are officially supported by
Github and will automatically be enabled on your website. However, the versions
may not be up-to-date. Check the versions
[here](https://pages.github.com/versions/). If you want to use the latest
versions of the plugins, you can use [Travis CI](https://travis-ci.org/) to
generate and deploy the website.

## Configuration

The template expects following variables in the `_config.yml`.

`lang`
: Langauge of the Website (for html tag)

`title`
: Title of the Website (for SEO and Header)

`author`
: Author of the Website (for SEO and Footer)

`url`
: Url of the website (for SEO)

`baseurl`
: [Optional] Baseurl of the Website (for relative URL)

`logo`
: [Optional] Logo of the Website (for SEO)

`copyright`
: [Optional] Copyright information for website (for Footer)

`mathjax`
: [Optional] Adds [MathJax](https://www.mathjax.org/) support \
  **Note**: Adds Javascript library

`syntax_highlight`
: [Optional] Adds Syntax highlighting support \
  **Note**: Links additional CSS file

[Example](https://github.com/simple-template/jekyll/blob/master/_config.yml)

## Layouts

The template provides following layouts:

* `default`
* `blog`
* `home`
* `page`
* `post`
* `redirect`
* `404`

The `default` layout implements a basic structure of the page. It is meant to
be used by other layouts as the base to build on to.

The `blog` layout implements a page listing all of the blog post. To make it
the front page of website, add the layout in `index.md` file at the root. If
you want Blog to be in a subdirectory like `/blog/`, you can create a file
`blog.md` in the root and add the following lines. The template will populate
the page with all the posts automatically. The content in the file will be
added before the posts.

```yaml
---
layout: blog
permalink: /blog/
---
```

[Example Page](https://st.argp.in/jekyll/blog/)

The `home` layout implements a home page with content and optionally a list of
latest blog posts. It is meant to be used as home page of the website. To
generate a home page, create a file `index.md` in the root and add the
following lines. The `posts` option controls if the list will be added or not.

```yaml
---
layout: home
posts: true
---
```

[Example Page](https://st.argp.in/jekyll/)

The `page` layout implements a generic page with content. It can be used for
any page on the website which is not a post. To add a page with `page` layout,
create a file with following lines in metadata. The `permalink` is recommended
for pages although not required.

```yaml
---
layout: page
---
```

The `post` layout implements the blog post page. It supports fields like `date`
and `description`. It is meant to be used for blog posts in `_posts` directory.
Note that tag support was removed from posts.

```yaml
---
layout: post
date: DATE_OF_POST
description: "DESCRIPTION FOR THE POST"
---
```

The `redirect` layout is sort of a hack to create *HTTP* redirects. If you move
a page or post from one place to another, it's a good idea to create a
redirect. But, a lot of static site hosting services do not provide a way to
create a redirect response. This layout provides a way to create redirects
irrespective of the hosting service. To create a redirect, add the markdown file
with following lines. This layout also respects the `permalink` field.

```yaml
---
layout: redirect
redirect: "URL"
---
```

A lot of static site hosting services including [Github
Pages](https://pages.github.com/), [Gitlab
Pages](https://about.gitlab.com/product/pages/), [Surge](https://surge.sh) and
[Netlify](https://www.netlify.com/) supports custom *404* pages. The `404`
layout can be used for that. To add a custom *404* page, create a file `404.md`
in the root and add the following lines. Note that `permalink` field is
required for this page. You can add a helpful message for visitors after the
following metadata.

```yaml
---
layout: 404
permalink: 404.html
---
```

[Example Page](https://st.argp.in/jekyll/404.html)

All the layouts respect `title` field. To override any of the built-in
layouts, just create the file with same name in `_layouts/` directory.

## Navigation

For navigation links, the template looks for `_data/navigation.yml` file. The
links should be added to the file in following format.

```yaml
- title: "Link Name"
  url: "http://example.com"
```

Where `url` can be external or relative.

[Example](https://github.com/simple-template/jekyll/blob/master/_data/navigation.yml)

## Social Links

For social links in footer, the template looks for `_data/social.yml` file.
Following is the supported social networks and format in which it should be
added.

```yaml
facebook : username
twitter  : username
github   : username
youtube  : channel
instagram: username
linkedin : username
```

[Example](https://github.com/simple-template/jekyll/blob/master/_data/social.yml)

## Stylesheets

The template adds a very basic style sheet into the generated *HTML* page's
`head` to prevent extra *HTTP* requests. To change the default style, create a
file `_includes/style.html` and add the lines between `<style>` tags. Jekyll
will automatically insert these into *HTML*.

The optional feature of syntax highlighting, uses external style sheet. To
overwrite the style, create a file `assets/highlight.css`.

## Micro.Blog

If you want to link the blog/site with [Micro.blog](https://micro.blog), add
following in `_config.yml`.

```yaml
micro_blog: username
```

This will generate a link tag in head (to verify site) and add a link to follow
on Blog page.

## Pocket

To use [Pocker for Publishers](https://getpocket.com/publisher/) with the
website, you need to add the Pocket verification code in the *HEAD* tag of the
website. The template uses a variable `pocket_verification` in the global
configuration to insert it into *HEAD*. To use this, add the following line in
`_config.yml`.

```yaml
pocket_verification: verification_code
```

## SEO

As mentioned above, the template uses
[jekyll-seo-tag](https://github.com/jekyll/jekyll-seo-tag/) for SEO
information. All the options from that can be used. For more information read
its
[Usage](https://github.com/jekyll/jekyll-seo-tag/blob/master/docs/usage.md).

## Sitemap and Feed

As mentioned above, the template uses
[jekyll-sitemap](https://github.com/jekyll/jekyll-sitemap/) and
[jekyll-feed](https://github.com/jekyll/jekyll-feed) for sitemap and feed
respectively. By default sitemap is available at `/sitemap.xml` and feed of
posts is available at `/feed.xml`. For more information about the plugins, check
out there repositories and read the documentations.

## Tests

<blockquote class="imgur-embed-pub" lang="en" data-id="a/F5WfYBi">
<a href="https://imgur.com/F5WfYBi">Simple Template — Jekyll Tests</a>
</blockquote>
<script async src="https://s.imgur.com/min/embed.js"></script>

## Links

* [Source](https://github.com/simple-template/jekyll)
* [Ruby Gem](https://rubygems.org/gems/jekyll-simple-template)

## License

[BSD 3-Clause](https://github.com/simple-template/jekyll/blob/master/LICENSE)
License.
