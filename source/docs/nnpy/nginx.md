---
title: Nnpy &mdash; Nginx
subtitle: Setting up Nginx proxy for Nnpy
---

[Argp](/index.html) / [Docs](/docs/index.html) / [Nnpy](/docs/nnpy/index.html)
/ Nginx

After installing Nnpy, a proxy server can be used to forward requests to nnpy
server. This section provides information to configure
[Nginx](https://www.nginx.org/) as a proxy server under
[Debian](https://www.debian.org/).

Add a new file named `nnpy` under `/etc/nginx/sites-available/` directory for
Nnpy with the following content and modify the necessary things.

```nginx
server {
	listen 80;

	# SSL
	# listen 443 ssl;
	# ssl_certificate /etc/nginx/certs/debian/final.crt;
	# ssl_certificate_key /etc/nginx/certs/debian/debian.key;
	# ssl_client_certificate /etc/nginx/certs/debian/client-ca.crt;
	# ssl_verify_client optional;

	server_name example.com;

	location / {
		include /etc/nginx/uwsgi_params;
		uwsgi_pass unix:/var/run/nnpy.sock;
	}
}
```

To enable it so that Nginx can see and process it, create a symlink to this
file in `/etc/nginx/sites-enabled/` directory.

```bash
$ ln -s /etc/nginx/sites-enabled/nnpy /etc/nginx/sites-available/nnpy
```

The virtual host for Nnpy is enabled now. It is always a good thing to verify
the configuration for errors. Nginx command, if run with `-t` option does the
error checking.

```bash
$ nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

To apply the new configurations Nginx needs to reload.

```bash
$ service nginx reload
```
