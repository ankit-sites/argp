---
title: Nnpy &mdash; Docker
subtitle: Documentation of the docker image of Nnpy
---

[Argp](/index.html) / [Docs](/docs/index.html) / [Nnpy](/docs/nnpy/index.html)
/ Docker

For easier deployment, Nnpy Docker image can be used instead of manually
installing everything. The Docker image uses [Alpine
Linux](https://alpinelinux.org/) and is therefore very lightweight.

The repository includes a `Dockerfile` which can be used to build the Docker
image manually. To build the image execute the following command.

```bash
$ docker build -t nnpy .
```

Alternatively, you can pull the pre-built image from [Docker
Hub](https://hub.docker.com/r/ankitrgadiya/nnpy/).

```bash
$ docker pull ankitrgadiya/nnpy
```

Nnpy uses the [SQLite](https://www.sqlite.org/) database to store the data. To
use the image, you need a database in place.

Once, you have the image pulled/built and SQLite database in place, to run the
container you can execute the following command.

```bash
$ docker run -v /absolute/path/nnpy.db:/app/nnpy.db -p 8080:80 -d ankitrgadiya/nnpy
```

* `-v` option mounts the `nnpy.db` inside the container at `/app/nnpy.db`.
* `-p` option exposes port 80 of container and maps it to port 8080 of the
  machine, effectively serving Pastebin at port 8080 of the local machine.
* `-d` option makes the container run in daemon mode.
