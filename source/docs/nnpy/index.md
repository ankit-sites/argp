---
title: Nnpy
subtitle: A very simple Pastebin
---

[Argp](/index.html) / [Docs](/docs/index.html) / Nnpy

[![Docker Automated build](https://img.shields.io/docker/automated/ankitrgadiya/nnpy.svg)](https://hub.docker.com/r/ankitrgadiya/nnpy/builds/)
[![Docker Stars](https://img.shields.io/docker/stars/ankitrgadiya/nnpy.svg)](https://hub.docker.com/r/ankitrgadiya/nnpy/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ankitrgadiya/nnpy.svg)](https://hub.docker.com/r/ankitrgadiya/nnpy/)
[![Build Status](https://travis-ci.com/ankitrgadiya/nnpy.svg?branch=master)](https://travis-ci.com/ankitrgadiya/nnpy)
[![GitHub issues](https://img.shields.io/github/issues/ankitrgadiya/nnpy.svg)](https://github.com/ankitrgadiya/nnpy/issues)
[![GitHub last commit](https://img.shields.io/github/last-commit/ankitrgadiya/nnpy.svg)](https://github.com/ankitrgadiya/nnpy/commits)
[![GitHub](https://img.shields.io/github/license/ankitrgadiya/nnpy.svg)](https://github.com/ankitrgadiya/nnpy/blob/master/LICENSE)

Nnpy is a very simple Pastebin and a clone of [nnmm](https://nnmm.nl/). It
is implemented in [Python 3](https://www.python.org/) and uses the
[Flask](http://flask.pocoo.org/) web framework. To store data, it uses the
[SQLite](https://www.sqlite.org/) database. It does not include a form to
submit data. Instead, it relies on other software to send *POST* request
directly.

A simple example of pasting the output of `ls` command to Nnpy using
`curl`.

```bash
$ ls -l | curl --data-urlencode c@- https://example.com
https://example.com/U01mj
```

Another example of pasting the contents of a plain text file to nnpy by
redirecting the input to `curl` command.

```bash
$ curl --data-urlencode c@- https://exampe.com < plain.txt
https://example.com/jSnn4
```

* [Docker](/docs/nnpy/docker.html)
* [Nginx](/docs/nnpy/nginx.html)
* [Setup](/docs/nnpy/setup.html)

## Links

* [Github:ankitrgadiya/nnpy](https://github.com/ankitrgadiya/nnpy)

## License

[BSD 3-Clause](https://github.com/ankitrgadiya/nnpy/blob/master/LICENSE) License
