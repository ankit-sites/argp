---
title: Nnpy &mdash; Setup
subtitle: Setting up Nnpy locally and adding Systemd service
---

[Argp](/index.html) / [Docs](/docs/index.html) / [Nnpy](/docs/nnpy/index.html)
/ Setup

Nnpy requires packages under [Debian](https://www.debian.org/) distribution
(similar packages for other distributions).

* virtualenv
* python3
* sqlite3
* python3-dev (Build Only)
* build-essential (Build Only)

After installing packages, create a virtual environment under the root
directory of Nnpy and activate it.

```bash
$ virtualenv -p python3 env/
Already using interpreter /usr/bin/python3
Using base prefix '/usr'
New python executable in /app/env/bin/python3
Also creating executable in /app/env/bin/python
Installing setuptools, pkg_resources, pip, wheel...done.
$ source env/bin/activate
```

Now, install Python modules which are used by Nnpy.

* flask
* uwsgi

```bash
(env) $ pip3 install -r requirements.txt
```

The packages marked as “Build Only” in the list above are required to build the
dependencies and can be uninstalled later.

Now we need to create a database with the `pastes` table with two columns for
`id` and `data`.

```bash
$ cat nnpy.sql | sqlite3 nnpy.db
```

Nnpy is ready to run now. It can be run directly by starting the web server.
Alternatively, it can create a *UNIX* socket which can be used by proxy servers
like [Nginx](https://www.nginx.org/) to proxy the web requests to Nnpy. The
`wsgi.ini` file handles the configuration. Default settings will run it in
*HTTP* mode and start web server which will listen at port 5000.

Execute the subsequent command to test `uwsgi` in place.

```bash
(env) $ uwsgi --ini wsgi.ini
```

[Systemd](https://en.wikipedia.org/wiki/Systemd), which is the default init
system and service manager in a lot of popular Linux Distributions, use service
files to manage services. Please create a new file with the following content
and save it as `nnpy.service`.


```ini
[Unit]
Description=nnpy paste
After=network.target

[Service]
WorkingDirectory=/path/to/nnpy/
Environment='PATH=/path/to/nnpy/env/bin'
ExecStart=/path/to/nnpy/env/bin/uwsgi --ini nnpy.ini
Restart=on-failure
RestartSec=5s
KillSignal=SIGQUIT
Type=notify
StandardError=syslog
NotifyAccess=all
Group=www-data

[Install]
WantedBy=multi-user.target
```

To add the service file to Systemd, you need to copy the file into
`/etc/systemd/system/` folder.

```bash
$ cp nnpy.service /etc/systemd/system/
```

Now, Systemd daemon needs to reload to recognize and process the service file.

```bash
$ sudo systemctl daemon-reload
```

Now the web app can be run by executing the following command.

```bash
$ sudo service nnpy start
```

To set the service to run on boot, execute the following command.

```bash
$ sudo systemctl enable nnpy.service
```
