---
title: "Docs"
subtitle: "List of all the documentation hosted on this website"
---

[Argp](/index.html) / Docs

* [Docker](/docs/docker/index.html)
	* [Caddy](/docs/docker/caddy.html)
	* [Nnpy](/docs/docker/nnpy.html)
	* [Pandoc](/docs/docker/pandoc.html)
* [Nnpy](/docs/nnpy/index.html)
	* [Docker](/docs/nnpy/docker.html)
	* [Nginx](/docs/nnpy/nginx.html)
	* [Setup](/docs/nnpy/setup.html)
* [Project Euler](/docs/project-euler/index.html)
	* [Problem 1](/docs/project-euler/prob1.html)
	* [Problem 2](/docs/project-euler/prob2.html)
* [Simple Template](/docs/simple-template/index.html)
	* [Jekyll](/docs/simple-template/jekyll.html)
	* [Pandoc](/docs/simple-template/pandoc.html)
