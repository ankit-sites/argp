---
title: Docker &mdash; Pandoc
subtitle: Docker image for Pandoc
---

[Argp](/index.html) / [Docs](/docs/index.html) /
[Docker](/docs/docker/index.html) / Pandoc

[![Docker Automated build](https://img.shields.io/docker/automated/ankitrgadiya/pandoc.svg)](https://hub.docker.com/r/ankitrgadiya/pandoc/builds/)
[![Docker Stars](https://img.shields.io/docker/stars/ankitrgadiya/pandoc.svg)](https://hub.docker.com/r/ankitrgadiya/pandoc/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ankitrgadiya/pandoc.svg)](https://hub.docker.com/r/ankitrgadiya/pandoc/)
[![GitHub issues](https://img.shields.io/github/issues/ankitrgadiya/docker-pandoc.svg)](https://github.com/ankitrgadiya/docker-pandoc/issues)
[![GitHub last commit](https://img.shields.io/github/last-commit/ankitrgadiya/docker-pandoc.svg)](https://github.com/ankitrgadiya/docker-pandoc/commits)
[![GitHub](https://img.shields.io/github/license/ankitrgadiya/docker-pandoc.svg)](https://github.com/ankitrgadiya/docker-pandoc/blob/master/LICENSE)


## About

The Pandoc image comes with the latest [Pandoc](https://pandoc.org/) binary
from [Github Releases](https://github.com/jgm/pandoc/releases). This image is
meant to be used by CI/CD services to generate various file formats supported
by Pandoc. However, this can also be used instead of distribution specific
versions of Pandoc to get the latest version.

## Usage

The `pandoc` command can be used by running the container.

```bash
$ docker run -it ankitrgadiya/pandoc pandoc <ARGS>
```

Although even for a very basic setup you'll probably need to mount the
directories in the container so the files that Pandoc writes can be persistent.
The `-v` option of `run` subcommand can be used to mount directories. To mount
the current directory in the container you can substitute `pwd` command in
place of directory. Note that by default the container starts in `/` directory
but you can use `-w` option to override the default working directory.

```bash
$ docker run -v `pwd`:/root/ -w="/root/" -it ankitrgadiya/pandoc pandoc <ARGS>
```

The image also comes with GNU Make installed so instead of running commands on
individual files, you can create a receipe in `Makefile` and run `make` command
instead.

```bash
$ docker run -v `pwd`:/root/ -w="/root/" -it ankitrgadiya/pandoc make
```

## Links

* [Source](https://github.com/ankitrgadiya/docker-pandoc)
* [Pre-Built Image](https://hub.docker.com/r/ankitrgadiya/pandoc)

## License

[BSD
3-Clause](https://github.com/ankitrgadiya/docker-pandoc/blob/master/LICENSE)
License.
