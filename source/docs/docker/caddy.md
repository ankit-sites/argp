---
title: Docker &mdash; Caddy
subtitle: Docker image for Caddy Web server
---

[Argp](/index.html) / [Docs](/docs/index.html) /
[Docker](/docs/docker/index.html) / Caddy

[![Docker Automated build](https://img.shields.io/docker/automated/ankitrgadiya/caddy.svg)](https://hub.docker.com/r/ankitrgadiya/caddy/builds/)
[![Docker Stars](https://img.shields.io/docker/stars/ankitrgadiya/caddy.svg)](https://hub.docker.com/r/ankitrgadiya/caddy/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ankitrgadiya/caddy.svg)](https://hub.docker.com/r/ankitrgadiya/caddy/)
[![GitHub issues](https://img.shields.io/github/issues/ankitrgadiya/docker-caddy.svg)](https://github.com/ankitrgadiya/docker-caddy/issues)
[![GitHub last commit](https://img.shields.io/github/last-commit/ankitrgadiya/docker-caddy.svg)](https://github.com/ankitrgadiya/docker-caddy/commits)
[![GitHub](https://img.shields.io/github/license/ankitrgadiya/docker-caddy.svg)](https://github.com/ankitrgadiya/docker-caddy/blob/master/LICENSE)

## About

The Caddy image comes with the latest [Caddy](https://pandoc.org/) binary. This
image can be used to serve static files locally or as a web server anywhere.

## Usage

The server can be started by running the container.

```bash
$ docker run -d -v `pwd`:/data -p 8000:8000 ankitrgadiya/caddy <ARGS>
```

The default arguments for the command are configured to surve from `/data`
directory inside the container. The server is configured to listen at port
8000. You can map it to any available port on the host system by changing the
`-p` flag of `docker` command.

All the arguments for `caddy` command can be passed to the container as well.

## Links

* [Source](https://github.com/ankitrgadiya/docker-caddy)
* [Pre-Built Image](https://hub.docker.com/r/ankitrgadiya/caddy)

## License

[BSD
3-Clause](https://github.com/ankitrgadiya/docker-caddy/blob/master/LICENSE)
License.
