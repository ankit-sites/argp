---
title: "PIA"
subtitle: "Go wrapper for PIA through OpenVPN"
header-includes:
	<meta name="go-import" content="argp.in/go/pia git https://gitlab.com/ankit-go/pia">
redirect: "https://gitlab.com/ankit-go/pia"
---
